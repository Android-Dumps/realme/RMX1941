#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/platform/bootdevice/by-name/recovery:33554432:2bdaefad449c6394f4ce8e892f2a058ca2dafd60; then
  applypatch  EMMC:/dev/block/platform/bootdevice/by-name/boot:33554432:d914a0f83121573fef72e0e1a88d76e04bdbdf30 EMMC:/dev/block/platform/bootdevice/by-name/recovery 2bdaefad449c6394f4ce8e892f2a058ca2dafd60 33554432 d914a0f83121573fef72e0e1a88d76e04bdbdf30:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
